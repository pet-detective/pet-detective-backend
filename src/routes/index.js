module.exports = (app, _opts, done) => {
  app.get('/health-check', (_request, reply) => {
    reply.send({ status: 'ok' });
  });

  done();
};
