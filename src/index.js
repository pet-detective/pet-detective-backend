const config = require('config');
const app = require('./app');

const { host, port } = config;

app.listen(port, host).then((address) => {
  app.log.info('server\t\t\t[%s]', app.chalk.magenta('ready'));
  app.log.info('address\t\t\t[%s]', app.chalk.magenta(address));

  app.log.debug(app.chalk.yellow('\n'));
  app.log.debug(app.chalk.yellow(app.printRoutes()));
}).catch((err) => {
  app.log.error('server\t\t\t[%s]', app.chalk.red('error'));

  throw err;
});
