# Pet Detective Backend

[![License: MIT](https://img.shields.io/badge/License-MIT-yellow.svg)](https://gitlab.com/gAmador/pet-detective-backend/-/blob/master/LICENSE)
[![Linter](https://img.shields.io/badge/Linter-ESLint-blueviolet.svg)](https://eslint.org/)
[![Style Guide](https://img.shields.io/badge/Style_Guide-airbnb-ff69b4.svg)](https://github.com/airbnb/javascript)

Pet Detective Backend.

## Requirements

- [NodeJS v14.15.5+](https://nodejs.org/en/) or [nvm v0.37.0+](https://github.com/nvm-sh/nvm/releases/tag/v0.37.0)

## License

[MIT.](./LICENSE) Copyright (c)
