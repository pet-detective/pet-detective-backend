const dotenv = require('dotenv');
const Joi = require('joi');

dotenv.config();

const validationSchema = Joi.object({
  // env
  NODE_ENV: Joi
    .string()
    .valid('development', 'production', 'testing')
    .required(),

  // host
  HOST: Joi
    .alternatives(
      Joi.string().ip(),
      Joi.string().hostname(),
    ).required(),
  PORT: Joi
    .number()
    .port()
    .required(),
});

const validationOptions = {
  abortEarly: false,
  stripUnknown: true,
};

const { error, value } = validationSchema.validate(process.env, validationOptions);

if (error) {
  // eslint-disable-next-line no-console
  console.error('config validation error: ');
  throw error;
}

module.exports = {
  // env
  environment: value.NODE_ENV,

  // host
  host: value.HOST,
  port: value.PORT,

  // logger
  logger: {
    redact: ['req.headers.authorization'],
  },
};
